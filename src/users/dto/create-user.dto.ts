import { IsEmpty, MinLength, Matches } from 'class-validator';
export class CreateUserDto {
  @IsEmpty()
  @MinLength(5)
  login: string;
  @IsEmpty()
  @MinLength(5)
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsEmpty()
  password: string;
}
