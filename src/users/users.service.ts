import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, login: 'admin', name: ' Administrator', password: 'pass@1234' },
  { id: 2, login: 'user1', name: ' user1', password: 'pass@1234' },
  { id: 3, login: 'user2', name: ' user2', password: 'pass@1234' },
];
let lastUserId = 4;

@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    console.log({ ...createUserDto });
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto, // login,name,password
    };
    users.push(newUser);
    return newUser;

    // const newUser = new User();
    // newUser.id = lastUserId++;
    // newUser.login = createUserDto.login;
    // newUser.name = createUserDto.name;
    // newUser.password = createUserDto.password;
    // users.push(newUser);
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = users[index];
    users.splice(index, 1);
    return `This action removes a #${id} user`;
  }

  reset() {
    users = [
      { id: 1, login: 'admin', name: ' Administrator', password: 'pass@1234' },
      { id: 2, login: 'user1', name: ' user1', password: 'pass@1234' },
      { id: 3, login: 'user2', name: ' user2', password: 'pass@1234' },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
